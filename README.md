# Description
This repository contains the pipeline files to test linux kernel.

This repository is based upon Linaro/users/naresh.kamboju/linux-pipeline.

# Dependencies
Linaro/lkft/pipelines/common that needs to be included into gitlab-ci.yml file
and also a few local files like builds.yml tests.yml, sanity_job.yml and
tuxconfig.yml

# Usage
The goal is to only change the builds.yml, tests.yml, sanity_job.yml and
tuxconfig.yml files to sure your needs.
